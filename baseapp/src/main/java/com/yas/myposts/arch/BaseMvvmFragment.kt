package com.yas.myposts.arch

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.addTo
import org.koin.androidx.viewmodel.ext.android.viewModel
import kotlin.reflect.KClass

abstract class BaseMvvmFragment<T : BaseViewModel>(viewModelClass: KClass<T>) : Fragment() ,
    Launcher {

    val viewModel: T by viewModel(viewModelClass)

    private val disposableView = DisposablesForLifecycle().apply { lifecycle.addObserver(this) }
    private val resultLambdas = mutableMapOf<Int, ActivityResultCallback?>()

    override fun onCreate(savedInstanceState: Bundle?) {
        disposableView.onBeforeCreate()
        super.onCreate(savedInstanceState)
    }

    override fun onStart() {
        disposableView.onBeforeStart()
        super.onStart()
    }

    override fun onResume() {
        disposableView.onBeforeResume()
        super.onResume()
    }

    fun Disposable.disposeOnPause() {
        this.addTo(disposableView.onPauseDisposable)
    }

    fun Disposable.disposeOnStop() {
        this.addTo(disposableView.onStopDisposable)
    }

    fun Disposable.disposeOnDestroy() {
        this.addTo(disposableView.onDestroyDisposable)
    }

    override fun context() = requireContext()

    override fun launch(intent: Intent) {
        startActivity(intent)
    }

    override fun launchForResult(intent: Intent, code: Int, onResult: ActivityResultCallback?) {
        resultLambdas[code] = onResult
        startActivityForResult(intent, code)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        resultLambdas[requestCode]?.let {
            it(resultCode, data)
        }
    }

    protected fun <T> LiveData<T>.observe(callback: (T) -> Unit) {
        this.observe(viewLifecycleOwner, Observer { callback(it) })
    }
}
