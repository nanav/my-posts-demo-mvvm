package com.yas.myposts.utils

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.google.android.material.snackbar.Snackbar
import android.view.View
import com.yas.myposts.arch.BaseMvvmActivity
import com.yas.myposts.baseapp.R
import com.yas.myposts.data.managers.RxConnectivity
import com.yas.myposts.ext.rx.applyIoSchedulers
import io.reactivex.disposables.Disposable
import org.koin.java.KoinJavaComponent.inject

class ConnectivitySnackbar(private val view: View) : LifecycleObserver {

    private val rxConnectivity: RxConnectivity by inject(RxConnectivity::class.java)
    private var connected: Boolean = rxConnectivity.isConnected
    private var connectionDisposable: Disposable? = null

    init {
        (view.context as BaseMvvmActivity<*,*>).lifecycle.addObserver(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onResume() {
        connectionDisposable = rxConnectivity.watch()
                .applyIoSchedulers()
                .subscribe {
                    val prevStatus = connected
                    connected = it

                    if (prevStatus != connected) {
                        if (connected) noConnectionError.dismiss()
                        else noConnectionError.show()
                    }
                }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun onPause() {
        connectionDisposable?.dispose()
    }

    private val noConnectionError by lazy {
        val snackbar = Snackbar.make(view, R.string.generic_error_connection_message, Snackbar.LENGTH_INDEFINITE)
        snackbar.setAction(view.context.getString(R.string.generic_dismiss), { _ -> snackbar.dismiss() })
        snackbar
    }

    companion object {
        @JvmStatic
        fun start(view: View) {
            ConnectivitySnackbar(view)
        }
    }
}