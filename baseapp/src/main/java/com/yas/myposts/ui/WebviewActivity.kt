package com.yas.myposts.ui

import android.app.Activity
import android.content.Intent
import android.net.http.SslError
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.webkit.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import com.yas.myposts.arch.Launcher
import com.yas.myposts.baseapp.databinding.ActivityWebviewBinding
import com.yas.myposts.ext.android.optionalExtra
import timber.log.Timber

class WebviewActivity : AppCompatActivity() {

    private val layout by lazy { ActivityWebviewBinding.inflate(layoutInflater) }

    private val allowedDomains: Array<String>? by optionalExtra(ARG_DOMAINS)
    private val successUrls: Array<String>? by optionalExtra(ARG_SUCCESS_URLS)
    private val errorUrls: Array<String>? by optionalExtra(ARG_ERROR_URLS)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        setContentView(layout.root)
        setSupportActionBar(layout.webviewToolbar)
        ViewCompat.setOnApplyWindowInsetsListener(layout.webviewToolbar) { v, insets ->
            v.setPadding(0, insets.systemWindowInsetTop, 0, 0)
            insets.consumeSystemWindowInsets()
        }
        ViewCompat.setOnApplyWindowInsetsListener(layout.webviewNoConnection) { v, insets ->
            v.setPadding(0, insets.systemWindowInsetTop, 0, 0)
            insets.consumeSystemWindowInsets()
        }

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        layout.webviewRetry.setOnClickListener {
            layout.webviewActual.reload()
            layout.webviewNoConnection.visibility = View.GONE
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        super.onResume()
        layout.webviewActual.webChromeClient = object: WebChromeClient() {
            override fun onProgressChanged(view: WebView?, newProgress: Int) {
                super.onProgressChanged(view, newProgress)
                layout.webviewProgress.isIndeterminate = false
                layout.webviewProgress.progress = newProgress
                if (newProgress == 100) {
                    layout.webviewProgress.visibility = View.GONE
                } else {
                    layout.webviewProgress.visibility = View.VISIBLE
                }
            }
        }

        layout.webviewActual.webViewClient = object: WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
                val shouldBlock = allowedDomains?.contains(request.url.host)?.not() ?: super.shouldOverrideUrlLoading(view, request)

                if (!shouldBlock) {
                    if (successUrls?.contains(request.url.toString()) == true) {
                        Timber.d("${request.url} blocked by domain list")
                        setResult(Activity.RESULT_OK)
                        finishAfterTransition()
                    }
                    if (errorUrls?.contains(request.url.toString()) == true) {
                        setResult(Activity.RESULT_FIRST_USER)
                        finishAfterTransition()
                    }
                } else {
                    Timber.d("${request.url} blocked by domain list")
                }

                return shouldBlock
            }

            override fun onPageFinished(view: WebView, url: String) {
                super.onPageFinished(view, url)
                supportActionBar!!.title = view.title
            }

            override fun onReceivedHttpError(view: WebView, request: WebResourceRequest, errorResponse: WebResourceResponse) {
                super.onReceivedHttpError(view, request, errorResponse)
                Timber.e("HTTP Error: ${errorResponse.statusCode}")
                layout.webviewNoConnection.visibility = View.VISIBLE
            }

            override fun onReceivedSslError(view: WebView, handler: SslErrorHandler?, error: SslError?) {
                super.onReceivedSslError(view, handler, error)
                Timber.e("SSL Error: ${error?.primaryError}")
                layout.webviewNoConnection.visibility = View.VISIBLE
            }
        }

        layout.webviewActual.loadUrl(intent.getStringExtra(ARG_URL))
    }

    companion object {
        private const val ARG_URL = "url.string"
        private const val ARG_DOMAINS = "domains.string_list"
        private const val ARG_SUCCESS_URLS = "success.string_list"
        private const val ARG_ERROR_URLS = "error.string_list"

        /**
         * If allowedDomains are set then the browser will not load any page from outside those domains
         *
         * If the user visits a url in
         *   successUrls: the activity will finish with RESULT_OK
         *   errorUrls: the activity will finish with RESULT_FIRST_USER
         *
         * If the user closes the activity in any other way: the activity will finish with RESULT_CANCELLED
         */
        fun start(launcher: Launcher, url: String, allowedDomains: Array<String>? = null, successUrls: Array<String>? = null, errorUrls: Array<String>? = null) {
            val intent = Intent(launcher.context(), WebviewActivity::class.java)
            intent.putExtra(ARG_URL, url)
            intent.putExtra(ARG_DOMAINS, allowedDomains)
            intent.putExtra(ARG_SUCCESS_URLS, successUrls)
            intent.putExtra(ARG_ERROR_URLS, errorUrls)
            launcher.launch(intent)
        }
    }
}
