package com.yas.myposts

import android.app.Application
import android.os.Looper
import com.yas.myposts.arch.ApiModule
import com.yas.myposts.arch.DataModule
import com.yas.myposts.arch.DatabaseModule
import com.yas.myposts.arch.PreferenceModule
import com.yas.myposts.ext.TimberKoinLogger
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.android.schedulers.AndroidSchedulers
import net.danlew.android.joda.JodaTimeAndroid
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.loadKoinModules
import org.koin.core.context.startKoin
import timber.log.Timber

open class MyPostsApp : Application() {

    override fun onCreate() {

        super.onCreate()

        startKoin {
            logger(TimberKoinLogger())
            androidContext(this@MyPostsApp)
        }

        loadKoinModules(
            listOf(
                AppModule,
                ViewModelModule,
                ApiModule,
                DataModule,
                DatabaseModule,
                PreferenceModule
            )
        )

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        JodaTimeAndroid.init(this)

        //Reportedly improves the speed of RX by allowing RX to be executed outside of Android 16ms Main thread frame
        RxAndroidPlugins.setInitMainThreadSchedulerHandler {
            AndroidSchedulers.from(
                Looper.getMainLooper(),
                true
            )
        }
    }

}
