package com.yas.myposts.ui.posts

import androidx.lifecycle.MutableLiveData
import com.f2prateek.rx.preferences2.Preference
import com.yas.myposts.arch.BaseViewModel
import com.yas.myposts.data.managers.contract.DataManager
import com.yas.myposts.data.model.Post
import com.yas.myposts.ext.rx.asFlowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class PostListViewModel constructor(
    private val userIdPreference: Preference<Int>,
    private val dataManager: DataManager
) : BaseViewModel(), PostListMvvm.ViewModel {

    private var posts: List<Post> = emptyList()

    val postListDataState = MutableLiveData<PostListDataState>()
    val postListFlowState = MutableLiveData<PostListFlowState>()

    override fun onCreate() {
        postListDataState.value = PostListDataState.PostListLoading(true)
        subscription.add(
            dataManager.getAllPosts()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    posts = it
                    postListDataState.value =
                        PostListDataState.PostListData(it, userIdPreference.get())
                }, {
                    Timber.e(it)
                    postListDataState.value = PostListDataState.Error
                })
        )
        subscription.add(
            userIdPreference.asFlowable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    setupCurrentUserName()
                }, {
                    Timber.e(it)
                    postListDataState.value = PostListDataState.Error
                })
        )
    }

    private fun setupCurrentUserName() {
        subscription.add(
            dataManager.getUserById(userIdPreference.get())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    postListDataState.value = PostListDataState.SetupCurrentUser(it.username)
                }, {
                    Timber.e(it)
                    postListDataState.value = PostListDataState.Error
                })
        )
    }

    override fun changeUser() {
        postListFlowState.value = PostListFlowState.StartSelectUser
    }

    override fun onPostClicked(post: Post) {
        postListDataState.value = PostListDataState.PostSelected(post)
    }

    override fun onFilterClicked(checked: Boolean) {
        val filteredPosts =
            if (checked) {
                posts.filter { it.userId == userIdPreference.get() }
            } else {
                posts
            }

        postListDataState.value =
            PostListDataState.PostListData(filteredPosts, userIdPreference.get())
    }
}
