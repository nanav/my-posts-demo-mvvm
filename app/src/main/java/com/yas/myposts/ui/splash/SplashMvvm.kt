package com.yas.myposts.ui.splash

import com.yas.myposts.arch.BaseMvvm

interface SplashMvvm {

    interface ViewModel : BaseMvvm.ViewModel {
        fun onCreate()
    }
}


sealed class SplashDataState

sealed class SplashFlowState {
    class SplashStartApp(val animate: Boolean) : SplashFlowState()
}