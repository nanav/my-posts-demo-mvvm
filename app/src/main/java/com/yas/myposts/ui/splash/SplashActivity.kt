package com.yas.myposts.ui.splash

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.os.Bundle
import com.yas.myposts.arch.BaseMvvmActivity
import com.yas.myposts.databinding.ActivitySplashBinding
import com.yas.myposts.ui.posts.PostListActivity

class SplashActivity : BaseMvvmActivity<SplashViewModel, ActivitySplashBinding>(
    SplashViewModel::class,
    ActivitySplashBinding::inflate
) {
    private val ANIMATION_DURATION = 2000L

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.splashDataState.observe { processDataState(it) }
        viewModel.splashFlowState.observe { processFlowState(it) }

        viewModel.onCreate()
    }

    private fun processDataState(splashDataState: SplashDataState) {
        // empty
    }

    private fun processFlowState(splashFlowState: SplashFlowState) {
        when (splashFlowState) {
            is SplashFlowState.SplashStartApp -> {
                startApp(splashFlowState.animate)
            }
        }
    }


    private fun startApp(animate: Boolean) {
        if (animate) {
            layout.userOnboarding.animate()
                .alpha(0f)
                .setDuration(ANIMATION_DURATION)
                .setListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator) {
                        startPostListActivity()
                    }
                })
        } else {
            startPostListActivity()
        }
    }

    private fun startPostListActivity() {
        PostListActivity.start(this)
        finish()
    }
}
