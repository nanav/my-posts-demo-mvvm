package com.yas.myposts.ui.splash

import androidx.lifecycle.MutableLiveData
import com.f2prateek.rx.preferences2.Preference
import com.yas.myposts.arch.BaseViewModel
import com.yas.myposts.ext.rx.applyIoSchedulers
import io.reactivex.Maybe
import java.util.concurrent.TimeUnit

class SplashViewModel constructor(private val lastTimeOpenPreference: Preference<Long>) :
    BaseViewModel(), SplashMvvm.ViewModel {

    val splashDataState = MutableLiveData<SplashDataState>()

    val splashFlowState = MutableLiveData<SplashFlowState>()

    override fun onCreate() {
        val isFirstTime = lastTimeOpenPreference.get() == 0L
        lastTimeOpenPreference.set(System.currentTimeMillis())

        if (!isFirstTime) {
            splashFlowState.value = SplashFlowState.SplashStartApp(isFirstTime)
        } else {
            subscription.add(
                Maybe.empty<Int>()
                    .delay(2, TimeUnit.SECONDS)
                    .applyIoSchedulers()
                    .subscribe({}, {}, {
                        splashFlowState.value = SplashFlowState.SplashStartApp(isFirstTime)
                    })
            )
        }
    }
}
