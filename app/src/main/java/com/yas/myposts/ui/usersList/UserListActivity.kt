package com.yas.myposts.ui.usersList

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.yas.myposts.R
import com.yas.myposts.arch.BaseMvvmActivity
import com.yas.myposts.data.model.User
import com.yas.myposts.databinding.ActivityUserListBinding
import com.yas.myposts.databinding.ItemUserBinding
import com.yas.myposts.ui.SimpleAdapter
import com.yas.myposts.ui.usersList.UserListDataState.*
import com.yas.myposts.utils.ConnectivitySnackbar

class UserListActivity : BaseMvvmActivity<UserListViewModel, ActivityUserListBinding>(
    UserListViewModel::class,
    ActivityUserListBinding::inflate
) {

    private val userListAdapter by lazy {
        SimpleAdapter(
            ItemUserBinding::inflate,
            this::bindAdapterView
        ) { viewModel.onUserClicked(it) }
    }

    private var currentUserId: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        ConnectivitySnackbar.start(layout.userList)

        title = getString(R.string.user_list_title)

        setSupportActionBar(layout.userToolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_arrow_back)
        }

        layout.userList.layoutManager = LinearLayoutManager(this)
        layout.userList.adapter = userListAdapter

        layout.userRefresh.setColorSchemeResources(R.color.color_accent)
        layout.userRefresh.setOnRefreshListener { viewModel.onCreate() }

        viewModel.userListDataState.observe { processDataState(it) }
        viewModel.userListFlowState.observe { processFlowState(it) }

        viewModel.onCreate()
    }

    private fun processDataState(userListDataState: UserListDataState) {
        when (userListDataState) {
            is UserListLoading -> showProgress(userListDataState.isLoading)
            is UserListData -> setData(
                userListDataState.users,
                userListDataState.userId
            )
            is RefreshCurrentUser -> refreshCurrentUserId(userListDataState.userId)
            is Error -> showError()
        }
    }

    private fun processFlowState(userListFlowState: UserListFlowState) {
        // empty
    }

    private fun showProgress(isLoading: Boolean) {
        layout.userList.visibility = if (isLoading) View.GONE else View.VISIBLE
        layout.userListProgress.visibility = if (isLoading) View.VISIBLE else View.GONE
    }

    private fun setData(users: List<User>, userId: Int) {
        layout.userListEmpty.visibility = if (users.isEmpty()) View.VISIBLE else View.GONE
        layout.userList.visibility = if (users.isEmpty()) View.GONE else View.VISIBLE
        layout.userListProgress.visibility = View.GONE
        layout.userRefresh.isRefreshing = false

        userListAdapter.setItems(users, { old, new -> old.id == new.id })

        this.currentUserId = userId
    }

    private fun bindAdapterView(
        holder: SimpleAdapter<User, ItemUserBinding>.SimpleViewHolder,
        user: User
    ) {
        if (user.id == currentUserId) {
            holder.layout.itemUserSelected.visibility = View.VISIBLE
            holder.layout.itemUserUnselected.visibility = View.GONE

            holder.layout.userNameSelected.text = user.name
            holder.layout.userUsernameSelected.text = user.username
            holder.layout.userEmailSelected.text = user.email
            holder.layout.userPhoneSelected.text = user.phone
            holder.layout.userWebsiteSelected.text = user.website

            AnimationUtils.loadAnimation(this, R.anim.shake).also { hyperspaceJumpAnimation ->
                holder.layout.root.startAnimation(hyperspaceJumpAnimation)
            }

        } else {
            holder.layout.itemUserUnselected.visibility = View.VISIBLE
            holder.layout.itemUserSelected.visibility = View.GONE
            holder.layout.userName.text = user.name
        }
    }

    private fun refreshCurrentUserId(userId: Int) {
        this.currentUserId = userId
        userListAdapter.notifyDataSetChanged()
    }

    private fun showError() {
        showProgress(false)
        Snackbar.make(layout.userList, getString(R.string.generic_error), Snackbar.LENGTH_SHORT)
            .show()
    }

    override fun onOptionsItemSelected(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            android.R.id.home -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(menuItem)
    }

    companion object {
        @JvmStatic
        fun start(context: Activity) {
            val intent = Intent(context, UserListActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
            context.startActivity(intent)
        }
    }
}
