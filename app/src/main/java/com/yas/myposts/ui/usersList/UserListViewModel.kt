package com.yas.myposts.ui.usersList

import androidx.lifecycle.MutableLiveData
import com.f2prateek.rx.preferences2.Preference
import com.yas.myposts.arch.BaseViewModel
import com.yas.myposts.data.managers.contract.DataManager
import com.yas.myposts.data.model.User
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class UserListViewModel constructor(
    private val lastTimeOpenPreference: Preference<Long>,
    private val userIdPreference: Preference<Int>,
    private val dataManager: DataManager
) : BaseViewModel(), UsersListMvvm.ViewModel {

    val userListDataState = MutableLiveData<UserListDataState>()
    val userListFlowState = MutableLiveData<UserListFlowState>()

    override fun onCreate() {
        userListDataState.value = UserListDataState.UserListLoading(true)

        subscription.add(
            dataManager.getAllUsers()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.find { it.id == userIdPreference.get() } == null) {
                        userListDataState.value =
                            UserListDataState.UserListData(listOf(), userIdPreference.get())
                        Timber.d("The list of users doesn't contain the current user")
                    } else {
                        userListDataState.value =
                            UserListDataState.UserListData(it, userIdPreference.get())
                    }
                }, {
                    Timber.e(it)
                    userListDataState.value = UserListDataState.Error
                })
        )
        lastTimeOpenPreference.set(System.currentTimeMillis())
    }

    override fun onUserClicked(user: User) {
        userListDataState.value = UserListDataState.RefreshCurrentUser(user.id)
        userIdPreference.set(user.id)
    }
}
