package com.yas.myposts.ui.posts

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import androidx.annotation.NonNull
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.snackbar.Snackbar
import com.jakewharton.rxbinding2.view.RxView
import com.yas.myposts.R
import com.yas.myposts.arch.BaseMvvmActivity
import com.yas.myposts.data.model.Post
import com.yas.myposts.databinding.ActivityPostListBinding
import com.yas.myposts.databinding.ItemPostBinding
import com.yas.myposts.ui.SimpleAdapter
import com.yas.myposts.ui.posts.PostListDataState.*
import com.yas.myposts.ui.posts.PostListFlowState.StartSelectUser
import com.yas.myposts.ui.usersList.UserListActivity
import com.yas.myposts.utils.ConnectivitySnackbar

class PostListActivity : BaseMvvmActivity<PostListViewModel, ActivityPostListBinding>(
    PostListViewModel::class,
    ActivityPostListBinding::inflate
) {

    private val postsAdapter by lazy {
        SimpleAdapter(
            ItemPostBinding::inflate,
            this::bindAdapterView
        ) { viewModel.onPostClicked(it) }
    }

    private lateinit var sheetBehavior: BottomSheetBehavior<LinearLayout>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        ConnectivitySnackbar.start(layout.postListContent)

        layout.postList.layoutManager = LinearLayoutManager(this)
        layout.postList.adapter = postsAdapter

        layout.postRefresh.setColorSchemeResources(R.color.color_accent)
        layout.postRefresh.setOnRefreshListener { viewModel.onCreate() }
        layout.changeUser.setOnClickListener { viewModel.changeUser() }

        sheetBehavior = BottomSheetBehavior.from(layout.bottomSheetLayout.postBottomSheet)
        sheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(@NonNull bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_COLLAPSED -> sheetBehavior.state =
                        BottomSheetBehavior.STATE_HIDDEN
                }
            }

            override fun onSlide(@NonNull bottomSheet: View, slideOffset: Float) {

            }
        })
        sheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN

        RxView.clicks(layout.postFilter)
            .subscribe { viewModel.onFilterClicked(layout.postFilter.isChecked) }
            .disposeOnStop()

        viewModel.postListDataState.observe { processDataState(it) }
        viewModel.postListFlowState.observe { processFlowState(it) }

        viewModel.onCreate()
    }

    private fun processDataState(postListDataState: PostListDataState) {
        when (postListDataState) {
            is PostListLoading -> showProgress(postListDataState.isLoading)
            is PostListData -> setData(
                postListDataState.posts,
                postListDataState.userId
            )
            is PostSelected -> showPost(postListDataState.post)
            is SetupCurrentUser -> setCurrentUser(postListDataState.name)
            is Error -> showError()
        }
    }

    private fun processFlowState(postListFlowState: PostListFlowState) {
        when (postListFlowState) {
            is StartSelectUser -> startUserList()
        }
    }

    private fun bindAdapterView(
        holder: SimpleAdapter<Post, ItemPostBinding>.SimpleViewHolder,
        post: Post
    ) {

        holder.layout.postTitle.text = post.title
        holder.layout.postAuthorName.text = post.getAttachedUser()?.name
        holder.layout.postAuthorUsername.text =
            getString(R.string.user_username, post.getAttachedUser()?.username)
    }

    private fun startUserList() {
        UserListActivity.start(this)
    }

    private fun showProgress(isLoading: Boolean) {
        layout.postListContent.visibility = if (isLoading) View.GONE else View.VISIBLE
        layout.postListProgress.visibility = if (isLoading) View.VISIBLE else View.GONE
    }

    private fun setData(posts: List<Post>, userId: Int) {
        layout.postListEmpty.visibility = if (posts.isEmpty()) View.VISIBLE else View.GONE
        layout.postListContent.visibility = if (posts.isEmpty()) View.GONE else View.VISIBLE
        layout.postListProgress.visibility = View.GONE
        layout.postRefresh.isRefreshing = false

        postsAdapter.setItems(posts)
    }

    private fun setCurrentUser(name: String) {
        layout.postsTitle.text = getString(R.string.posts_list_welcome)
        layout.postsUsername.text = getString(R.string.posts_list_welcome_username, name)
    }

    private fun showPost(post: Post) {
        layout.bottomSheetLayout.postAuthorName.text = post.getAttachedUser()?.name
        layout.bottomSheetLayout.postAuthorUsername.text = getString(R.string.user_username, post.getAttachedUser()?.username)
        layout.bottomSheetLayout.postTitle.text =  getString(R.string.user_title, post.title)
        layout.bottomSheetLayout.postBody.text = post.body

        sheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
    }

    private fun showError() {
        showProgress(false)
        Snackbar.make(
            layout.postListContent,
            getString(R.string.generic_error),
            Snackbar.LENGTH_SHORT
        )
            .show()
    }

    override fun finish() {
        setResult(Activity.RESULT_OK)
        super.finish()
    }

    companion object {
        @JvmStatic
        fun start(context: Activity) {
            val intent = Intent(context, PostListActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
            context.startActivity(intent)
        }
    }
}
