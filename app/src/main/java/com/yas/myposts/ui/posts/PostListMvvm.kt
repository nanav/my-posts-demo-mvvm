package com.yas.myposts.ui.posts

import com.yas.myposts.arch.BaseMvvm
import com.yas.myposts.data.model.Post

interface PostListMvvm {

    interface ViewModel : BaseMvvm.ViewModel {
        fun onCreate()
        fun changeUser()
        fun onPostClicked(post: Post)
        fun onFilterClicked(checked: Boolean)
    }
}

sealed class PostListDataState {
    object Error : PostListDataState()
    class PostListLoading(val isLoading: Boolean) : PostListDataState()
    class SetupCurrentUser(val name: String) : PostListDataState()
    class PostSelected(val post: Post) : PostListDataState()
    class PostListData(val posts: List<Post>, val userId: Int) : PostListDataState()
}

sealed class PostListFlowState {
    object StartSelectUser : PostListFlowState()
}
