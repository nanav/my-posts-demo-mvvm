package com.yas.myposts.ui.usersList

import com.yas.myposts.arch.BaseMvvm
import com.yas.myposts.data.model.User

interface UsersListMvvm {

    interface ViewModel : BaseMvvm.ViewModel {
        fun onCreate()
        fun onUserClicked(user: User)
    }

}

sealed class UserListDataState {
    object Error : UserListDataState()
    class UserListLoading(val isLoading: Boolean) : UserListDataState()
    class RefreshCurrentUser(val userId: Int) : UserListDataState()
    class UserListData(val users: List<User>, val userId: Int) : UserListDataState()
}

sealed class UserListFlowState