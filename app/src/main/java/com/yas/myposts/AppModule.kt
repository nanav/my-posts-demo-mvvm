package com.yas.myposts

import com.yas.myposts.arch.PREF_LAST_TIME_OPEN
import com.yas.myposts.arch.PREF_USER_ID
import com.yas.myposts.ui.posts.PostListViewModel
import com.yas.myposts.ui.splash.SplashViewModel
import com.yas.myposts.ui.usersList.UserListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module

val AppModule = module {

    single {
        // add here any dependant module
    }

}


val ViewModelModule = module {

    viewModel {
        SplashViewModel(
            lastTimeOpenPreference = get(named(PREF_LAST_TIME_OPEN))
        )
    }

    viewModel {
        UserListViewModel(
            lastTimeOpenPreference = get(named(PREF_LAST_TIME_OPEN)),
            userIdPreference = get(named(PREF_USER_ID)),
            dataManager = get()
        )
    }

    viewModel {
        PostListViewModel(
            userIdPreference = get(named(PREF_USER_ID)),
            dataManager = get()
        )
    }

}

