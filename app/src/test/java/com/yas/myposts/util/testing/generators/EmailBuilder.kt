package com.yas.myposts.util.testing.generators

import com.yas.myposts.util.testing.DataGenerator
import com.yas.myposts.util.testing.FixtureBuilder

class EmailBuilder(dataGenerator: DataGenerator) : FixtureBuilder<String>(dataGenerator) {

    internal var output: String? = null

    fun reset(): EmailBuilder {
        output = null
        return this
    }

    override fun build(): String {
        if (output == null) {
            output = dataGenerator.nextEmail()
        }
        return output!!
    }

    fun setEmail(email: String): EmailBuilder {
        this.output = email
        return this
    }

}
