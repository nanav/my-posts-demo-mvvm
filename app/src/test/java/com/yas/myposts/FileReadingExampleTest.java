package com.yas.myposts;

import org.junit.Test;

import java.util.Scanner;

import static org.junit.Assert.assertEquals;

public class FileReadingExampleTest {

    @Test
    public void readFile() {
        Scanner scanner = new Scanner(getClass().getClassLoader().getResourceAsStream("file_reading_test.txt"));
        scanner.useDelimiter("\\Z");
        String contents = scanner.next();
        scanner.close();

        assertEquals("test_file_output", contents);
    }
}
