package com.yas.myposts

import android.app.Application
import com.yas.myposts.util.testing.TestTree
import timber.log.Timber

class TestMyPostsApp : Application() {

    override fun onCreate() {
        super.onCreate()
        Timber.plant(TestTree())
    }

}
