package com.yas.myposts.ui.resources

import android.content.Intent
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import com.yas.myposts.R
import com.yas.myposts.RecyclerViewMatcher
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ResourcesActivityTest {
    @Rule
    var activityRule = ActivityTestRule(ResourcesActivity::class.java, true, false)

    @Test
    fun checkRecyclerView() {
        activityRule.launchActivity(Intent())

        val resource1 = MockResources.ALL_RESOURCES[0]
        onView(RecyclerViewMatcher.withRecyclerView(R.id.resources_list).atPositionOnView(0, R.id.resource_initials))
                .check(matches(withText(resource1.initials())))
        onView(RecyclerViewMatcher.withRecyclerView(R.id.resources_list).atPositionOnView(0, R.id.resource_name))
                .check(matches(withText(resource1.name)))
        onView(RecyclerViewMatcher.withRecyclerView(R.id.resources_list).atPositionOnView(0, R.id.resource_type))
                .check(matches(withText(resource1.type)))

        val resource2 = MockResources.ALL_RESOURCES[1]
        onView(RecyclerViewMatcher.withRecyclerView(R.id.resources_list).atPositionOnView(1, R.id.resource_initials))
                .check(matches(withText(resource2.initials())))
        onView(RecyclerViewMatcher.withRecyclerView(R.id.resources_list).atPositionOnView(1, R.id.resource_name))
                .check(matches(withText(resource2.name)))
        onView(RecyclerViewMatcher.withRecyclerView(R.id.resources_list).atPositionOnView(1, R.id.resource_type))
                .check(matches(withText(resource2.type)))

        val resource3 = MockResources.ALL_RESOURCES[2]
        onView(RecyclerViewMatcher.withRecyclerView(R.id.resources_list).atPositionOnView(2, R.id.resource_initials))
                .check(matches(withText(resource3.initials())))
        onView(RecyclerViewMatcher.withRecyclerView(R.id.resources_list).atPositionOnView(2, R.id.resource_name))
                .check(matches(withText(resource3.name)))
        onView(RecyclerViewMatcher.withRecyclerView(R.id.resources_list).atPositionOnView(2, R.id.resource_type))
                .check(matches(withText(resource3.type)))

    }
}
