package com.yas.myposts.ui.login


import android.view.View
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions.clearText
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.closeSoftKeyboard
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withParent
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import com.yas.myposts.R
import org.hamcrest.Matchers.allOf
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class LoginActivityTest {

    private var usernameToBeTyped: String? = null
    private var pwdToBeTyped: String? = null
    private var usernameView: ViewInteraction? = null
    private var passwordTextView: ViewInteraction? = null
    private var loginButton: ViewInteraction? = null

    @get:Rule
    var mActivityRule = ActivityTestRule(LoginActivity::class.java)

    @Before
    fun init() {
        usernameToBeTyped = "user@mubaloo.com"
        pwdToBeTyped = "password"
        usernameView = onView(allOf<View>(withId(R.id.login_username)))
        passwordTextView = onView(allOf<View>(withId(R.id.login_password)))
        loginButton = onView(allOf<View>(withId(R.id.login_action), withText(R.string.login), withParent(withId(R.id.login_content_group))))
    }

    @Test
    fun test_username() {
        usernameView!!.check(matches(isDisplayed()))
        usernameView!!.perform(click(), clearText(), typeText(usernameToBeTyped!!), closeSoftKeyboard())
        usernameView!!.check(matches(withText(usernameToBeTyped)))
    }

    @Test
    fun test_password() {
        passwordTextView!!.check(matches(isDisplayed()))
        passwordTextView!!.perform(click(), clearText(), typeText(usernameToBeTyped!!), closeSoftKeyboard())
        passwordTextView!!.check(matches(withText(usernameToBeTyped)))
    }

    @Test
    fun test_login() {
        usernameView!!.check(matches(isDisplayed()))
        usernameView!!.perform(click(), clearText(), typeText(usernameToBeTyped!!), closeSoftKeyboard())
        usernameView!!.check(matches(withText(usernameToBeTyped)))

        passwordTextView!!.check(matches(isDisplayed()))
        passwordTextView!!.perform(click(), clearText(), typeText(pwdToBeTyped!!), closeSoftKeyboard())
        passwordTextView!!.check(matches(withText(pwdToBeTyped)))

        loginButton!!.check(matches(isDisplayed()))
        loginButton!!.perform(click())
        onView(allOf<View>(withId(R.id.login_progress))).check(matches(isDisplayed()))
    }
}
