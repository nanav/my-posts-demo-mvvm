package com.yas.myposts.data.database.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.yas.myposts.data.model.Post
import io.reactivex.Flowable

internal const val POST_TABLE_NAME = "posts"
internal const val POST_ID = "_id"
internal const val POST_WEB_ID = "id"
internal const val POST_USER_ID = "user_id"
internal const val POST_TITLE = "title"
internal const val POST_BODY = "body"

@Dao
interface PostDao {
    @Query("SELECT * FROM $POST_TABLE_NAME ORDER BY $POST_WEB_ID DESC")
    fun getAll(): Flowable<List<Post>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(posts: List<Post>): List<Long>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(user: Post): Long

    @Delete
    fun delete(user: Post): Int

    @Query("SELECT * FROM $POST_TABLE_NAME WHERE $POST_WEB_ID = :id")
    fun get(id: Long): Flowable<Post>

    @Query("SELECT * FROM $POST_TABLE_NAME WHERE $POST_USER_ID = :id")
    fun getByUserId(id: Long): Flowable<List<Post>>

    @Query("DELETE FROM $POST_TABLE_NAME")
    fun deleteTableContent()
}
