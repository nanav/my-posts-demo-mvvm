package com.yas.myposts.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.yas.myposts.data.database.dao.POST_BODY
import com.yas.myposts.data.database.dao.POST_ID
import com.yas.myposts.data.database.dao.POST_TABLE_NAME
import com.yas.myposts.data.database.dao.POST_TITLE
import com.yas.myposts.data.database.dao.POST_USER_ID
import com.yas.myposts.data.database.dao.POST_WEB_ID

@Entity(tableName = POST_TABLE_NAME)
data class Post(@PrimaryKey(autoGenerate = true)
                @ColumnInfo(name = POST_ID) val localId: Long,
                @ColumnInfo(name = POST_WEB_ID) val id: Int,
                @ColumnInfo(name = POST_USER_ID) val userId: Int,
                @ColumnInfo(name = POST_TITLE) val title: String,
                @ColumnInfo(name = POST_BODY) val body: String) {
    @Ignore
    var user: User? = null

    fun attachUser(user: User?): Post {
        this.user = user
        return this
    }

    fun getAttachedUser(): User? {
        return user
    }
}