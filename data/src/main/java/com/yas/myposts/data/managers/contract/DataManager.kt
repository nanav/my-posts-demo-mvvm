package com.yas.myposts.data.managers.contract

import com.yas.myposts.data.model.Post
import com.yas.myposts.data.model.User
import io.reactivex.Flowable

interface DataManager {
    fun getAllPosts(): Flowable<List<Post>>
    fun getAllUsers(): Flowable<List<User>>
    fun getUserById(userId: Int): Flowable<User>
    fun getPostsByUserId(userId: Int): Flowable<List<Post>>
}
