package com.yas.myposts.data.database

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.yas.myposts.data.model.User

class Converters {
    private val gson = Gson()

    @TypeConverter
    fun toUserAddress(data: String?): User.UserAddress? {
        if (data == null) return data

        return gson.fromJson(data, object : TypeToken<User.UserAddress>() {}.type)
    }

    @TypeConverter
    fun toUserCompany(data: String?): User.UserCompany? {
        if (data == null) return data

        return gson.fromJson(data, object : TypeToken<User.UserCompany>() {}.type)
    }

    @TypeConverter
    fun fromUserAddress(someObject: User.UserAddress): String = gson.toJson(someObject)

    @TypeConverter
    fun fromUserCompany(someObject: User.UserCompany): String = gson.toJson(someObject)
}

