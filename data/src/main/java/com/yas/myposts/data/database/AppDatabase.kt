package com.yas.myposts.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.yas.myposts.data.database.dao.PostDao
import com.yas.myposts.data.database.dao.UserDao
import com.yas.myposts.data.model.Post
import com.yas.myposts.data.model.User

@Database(entities = [User::class, Post::class], version = 1, exportSchema = true)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
    abstract fun postDao(): PostDao
}
