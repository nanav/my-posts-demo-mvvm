package com.yas.myposts.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.yas.myposts.data.database.dao.USER_ADDRESS
import com.yas.myposts.data.database.dao.USER_COMPANY
import com.yas.myposts.data.database.dao.USER_EMAIL
import com.yas.myposts.data.database.dao.USER_ID
import com.yas.myposts.data.database.dao.USER_NAME
import com.yas.myposts.data.database.dao.USER_PHONE
import com.yas.myposts.data.database.dao.USER_TABLE_NAME
import com.yas.myposts.data.database.dao.USER_USERNAME
import com.yas.myposts.data.database.dao.USER_WEBSITE
import com.yas.myposts.data.database.dao.USER_WEB_ID

@Entity(tableName = USER_TABLE_NAME)
data class User(@PrimaryKey(autoGenerate = true)
                @ColumnInfo(name = USER_ID) val localId: Int,
                @ColumnInfo(name = USER_WEB_ID) val id: Int,
                @ColumnInfo(name = USER_NAME) val name: String,
                @ColumnInfo(name = USER_USERNAME) val username: String,
                @ColumnInfo(name = USER_EMAIL) val email: String,
                @ColumnInfo(name = USER_ADDRESS) val address: UserAddress,
                @ColumnInfo(name = USER_PHONE) val phone: String,
                @ColumnInfo(name = USER_WEBSITE) val website: String,
                @ColumnInfo(name = USER_COMPANY) val company: UserCompany) {

    data class UserAddress(val street: String,
                           val suite: String,
                           val city: String,
                           val zipcode: String,
                           val geo: GeoData)

    data class GeoData(val lat: String, val lng: String)

    data class UserCompany(val name: String, val catchPhrase: String, val bs: String)
}