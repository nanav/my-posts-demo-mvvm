package com.yas.myposts.data.database.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.yas.myposts.data.model.User
import io.reactivex.Flowable

internal const val USER_TABLE_NAME = "users"
internal const val USER_ID = "_id"
internal const val USER_WEB_ID = "id"
internal const val USER_NAME = "name"
internal const val USER_USERNAME = "username"
internal const val USER_EMAIL = "email"
internal const val USER_ADDRESS = "address"
internal const val USER_PHONE = "phone"
internal const val USER_WEBSITE = "website"
internal const val USER_COMPANY = "company"

@Dao
interface UserDao {
    @Query("SELECT * FROM $USER_TABLE_NAME ORDER BY $USER_NAME")
    fun getAll(): Flowable<List<User>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(users: List<User>): List<Long>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(user: User): Long

    @Delete
    fun delete(user: User): Int

    @Query("SELECT * FROM $USER_TABLE_NAME WHERE $USER_WEB_ID = :id")
    fun get(id: Long): Flowable<User>

    @Query("DELETE FROM $USER_TABLE_NAME")
    fun deleteTableContent()
}
