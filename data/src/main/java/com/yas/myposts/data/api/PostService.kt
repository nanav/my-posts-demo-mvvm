package com.yas.myposts.data.api

import com.yas.myposts.data.model.Post
import com.yas.myposts.data.model.User
import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface PostService {
    @GET("posts")
    fun getAllPosts(): Flowable<List<Post>>

    @GET("users")
    fun getAllUsers(): Flowable<List<User>>

    @GET("users/{userId}")
    fun getUserById(@Path("userId") userId: Int): Flowable<User>

    @GET("posts")
    fun getPostsByUserId(@Query("userId") userId: Int): Flowable<List<Post>>
}
