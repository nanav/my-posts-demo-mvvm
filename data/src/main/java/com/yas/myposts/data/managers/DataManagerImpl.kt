package com.yas.myposts.data.managers

import com.yas.myposts.data.api.PostService
import com.yas.myposts.data.database.dao.PostDao
import com.yas.myposts.data.database.dao.UserDao
import com.yas.myposts.data.managers.contract.DataManager
import com.yas.myposts.data.model.Post
import com.yas.myposts.data.model.User
import io.reactivex.Flowable
import io.reactivex.functions.BiFunction

class DataManagerImpl constructor(
    private val postService: PostService,
    private val userDao: UserDao,
    private val postDao: PostDao,
    private val connectionStatus: RxConnectivity
) : DataManager {

    private val combinePostUser = BiFunction<List<Post>, List<User>, List<Post>>
    { posts, users ->
        posts.shuffled().map { post ->
            post.attachUser(users.find { it.id == post.userId })
        }
    }

    override fun getAllPosts(): Flowable<List<Post>> {
        return if (connectionStatus.isConnected) {
            downloadFullPosts()
        } else {
            getFullPostDb()
        }
    }

    override fun getAllUsers(): Flowable<List<User>> {
        return if (connectionStatus.isConnected) {
            downloadUsers()
        } else {
            userDao.getAll()
        }
    }

    override fun getUserById(userId: Int): Flowable<User> {
        return if (connectionStatus.isConnected) {
            postService.getUserById(userId)
        } else {
            userDao.get(userId.toLong())
        }
    }

    override fun getPostsByUserId(userId: Int): Flowable<List<Post>> {
        return if (connectionStatus.isConnected) {
            postService.getPostsByUserId(userId)
        } else {
            postDao.getByUserId(userId.toLong())
        }
    }

    private fun downloadFullPosts(): Flowable<List<Post>> {
        return Flowable.zip(downloadPosts(), downloadUsers(), combinePostUser)
    }

    private fun getFullPostDb(): Flowable<List<Post>> {
        return Flowable.zip(postDao.getAll(), userDao.getAll(), combinePostUser)
    }

    private fun downloadPosts(): Flowable<List<Post>> {
        return postService.getAllPosts()
            .map {
                postDao.deleteTableContent()
                postDao.insertAll(it)
                it
            }
    }

    private fun downloadUsers(): Flowable<List<User>> {
        return postService.getAllUsers()
            .map {
                userDao.deleteTableContent()
                userDao.insertAll(it)
                it
            }
    }
}
