package com.yas.myposts.arch

import com.yas.myposts.data.managers.*
import com.yas.myposts.data.managers.contract.DataManager
import org.koin.dsl.module

val DataModule = module {

    single {
        RxConnectivity(
            context = get()
        )
    }

    single<DataManager> {
        DataManagerImpl(
            postService = get(),
            userDao = get(),
            postDao = get(),
            connectionStatus = get()
        )
    }

}
