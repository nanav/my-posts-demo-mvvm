package com.yas.myposts.arch

import androidx.room.Room
import com.yas.myposts.data.database.AppDatabase
import org.koin.core.context.KoinContextHandler
import org.koin.dsl.module

val DatabaseModule = module {

    val appDatabase: AppDatabase = Room.databaseBuilder(KoinContextHandler.get().get(), AppDatabase::class.java, "myposts_yas_db")
            .fallbackToDestructiveMigration()
            .build()

    single { appDatabase.userDao() }
    single { appDatabase.postDao() }

}
