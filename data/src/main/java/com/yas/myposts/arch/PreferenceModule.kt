package com.yas.myposts.arch

import androidx.preference.PreferenceManager
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys
import com.f2prateek.rx.preferences2.RxSharedPreferences
import org.koin.core.context.KoinContextHandler
import org.koin.core.qualifier.named
import org.koin.dsl.module

const val PREF_FUNC_CLEAR = "clearPrefs"
const val PREF_LAST_TIME_OPEN = "lastTimeOpenPref.long"
const val PREF_USER_ID = "userId.int"

val PreferenceModule = module {

    val sharedPrefs by lazy { PreferenceManager.getDefaultSharedPreferences(KoinContextHandler.get().get()) }

    val rxSharedPrefs by lazy { RxSharedPreferences.create(sharedPrefs) }

    val encryptedSharedPrefs by lazy {
        val keyGenParameterSpec = MasterKeys.AES256_GCM_SPEC
        val masterKeyAlias = MasterKeys.getOrCreate(keyGenParameterSpec)
        EncryptedSharedPreferences
            .create(
                "secure-prefs",
                masterKeyAlias,
                KoinContextHandler.get().get(),
                EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
            )
    }

    val encryptedRxSharedPrefs by lazy { RxSharedPreferences.create(encryptedSharedPrefs) }

    factory(named(PREF_FUNC_CLEAR)) {
        {
            rxSharedPrefs.clear()
            encryptedRxSharedPrefs.clear()
        }
    }

    single(named(PREF_LAST_TIME_OPEN)) {
        rxSharedPrefs.getLong("lastTimeOpen.long")
    }

    single(named(PREF_USER_ID)) {
        rxSharedPrefs.getInteger("userId.int", 1)
    }
}
