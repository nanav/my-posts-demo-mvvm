package com.yas.myposts.arch

import android.content.Context
import com.yas.myposts.data.BuildConfig
import com.yas.myposts.data.api.PostService
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber

private const val CACHE_SIZE = 10 * 1024 * 1024L //10MB
const val API_NON_AUTH_CLIENT = "OkHttpClientNoAuth"
const val API_AUTH_CLIENT = "OkHttpClientAuth"
const val RETROFIT_NON_AUTH = "RetrofitNoAuth"
const val RETROFIT_AUTH = "RetrofitAuth"
const val LOGGING_INTERCEPTOR = "loggingInterceptor"

val ApiModule = module {
    //services

    single { get<Retrofit>(named(RETROFIT_NON_AUTH)).create(PostService::class.java) }

    //retrofit
    single(named(RETROFIT_NON_AUTH)) {
        Retrofit.Builder()
            .baseUrl(BuildConfig.POST_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(get(named(API_NON_AUTH_CLIENT)))
            .build()
    }

    single(named(RETROFIT_AUTH)) {
        Retrofit.Builder()
            .baseUrl(BuildConfig.POST_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(get(named(API_AUTH_CLIENT)))
            .build()
    }


    //okhttp
    single(named(API_NON_AUTH_CLIENT)) {
        OkHttpClient.Builder()
            .addInterceptor(get<HttpLoggingInterceptor>(named(LOGGING_INTERCEPTOR)))
            .cache(Cache(get<Context>().cacheDir, CACHE_SIZE))
            .build()
    }

    single(named(API_AUTH_CLIENT)) {
        OkHttpClient.Builder()
            .addInterceptor(get<HttpLoggingInterceptor>(named(LOGGING_INTERCEPTOR)))
            .cache(Cache(get<Context>().cacheDir, CACHE_SIZE))
            .addNetworkInterceptor { chain ->
                var request = chain.request()
                request = request.newBuilder()
                    // TODO .addHeader("x-api-key", "123456789")
                    .build()

                chain.proceed(request)
            }
            .build()
    }

    //Okhttp Interceptors
    single(named(LOGGING_INTERCEPTOR)) {
        HttpLoggingInterceptor(object : HttpLoggingInterceptor.Logger {
            override fun log(message: String) {
                Timber.d(message)
            }
        }).apply {
            level = HttpLoggingInterceptor.Level.BODY
        }
    }
}
