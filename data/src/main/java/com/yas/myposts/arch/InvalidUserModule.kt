package com.yas.myposts.arch

import android.annotation.SuppressLint

@SuppressLint("StaticFieldLeak")
object InvalidUserModule {
    lateinit var onUserInvalid: () -> Unit
}
