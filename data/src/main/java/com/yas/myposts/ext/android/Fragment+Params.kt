package com.yas.myposts.ext.android

import androidx.fragment.app.Fragment

inline fun <reified T: Any> Fragment.optionalExtra(key: String) = lazy {
    val value = arguments?.get(key)
    if (value is T?) value else throw IllegalStateException("Activity param $key was wrong type (${(value!!)::class.java.simpleName})") //Force unwrap is fine as (null is Any?) == true
}

inline fun <reified T: Any> Fragment.extra(key: String) = lazy {
    if (arguments == null) {
        throw IllegalStateException("Arguments not set")
    }
    val value = arguments!!.get(key)
    value as? T ?: throw IllegalStateException("$key null or wrong type")
}
