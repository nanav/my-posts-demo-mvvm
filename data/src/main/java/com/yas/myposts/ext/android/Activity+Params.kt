package com.yas.myposts.ext.android

import android.app.Activity

inline fun <reified T: Any> Activity.optionalExtra(key: String) = lazy {
    val value = intent?.extras?.get(key)
    if (value is T?) value else throw IllegalStateException("Activity param $key was wrong type (${(value!!)::class.java.simpleName})") //Force unwrap is fine as (null is Any?) == true
}

inline fun <reified T: Any> Activity.extra(key: String) = lazy {
    if (intent == null || intent.extras == null) {
        throw IllegalStateException("Intent/extras not set")
    }
    val value = intent!!.extras!!.get(key)
    value as? T ?: throw IllegalStateException("$key null or wrong type")
}
