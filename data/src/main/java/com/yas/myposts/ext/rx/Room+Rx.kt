package com.yas.myposts.ext.rx

import io.reactivex.Single

fun List<Long>.wrap(): Single<List<Long>> {
    return Single.fromCallable { this }
}
