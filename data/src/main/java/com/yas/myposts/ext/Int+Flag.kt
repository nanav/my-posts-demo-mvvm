package com.yas.myposts.ext

fun Int.matches(flag: Int): Boolean {
    return (this and flag) == flag
}

fun Long.matches(flag: Long): Boolean {
    return (this and flag) == flag
}
