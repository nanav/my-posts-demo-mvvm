package com.yas.myposts.ext.android

import android.graphics.Canvas

fun Canvas.alter(func: () -> Unit) {
    save()
    func()
    restore()
}
